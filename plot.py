import pandas as pd
import seaborn as sns
from datamanager import DataManager
import matplotlib.pyplot as plt
import datetime
from utils import mkdir

sns.set_style("whitegrid")
sns.set_context("paper")

def save_plot(bairro, final_date, title, therm_size=512):
    predict_data = "cases_{}".format(bairro)
    historical_data = "data/{}_{}.csv".format(bairro, final_date)
    df = DataManager("data", predict_data, "th_{}".format(therm_size)).get_data()

    df["cases"] = df["y"]
    df["Description"] = df["mean"]
    df2 = pd.read_csv(historical_data)
    df2["Description"] = "Dados reais"

    fdf = pd.concat((df, df2[df2["cases"] >= df2["cases"].max() * 0.3]))
    fdf.drop(columns=["y", "lag", "n"], inplace=True)
    fdf["day"] = fdf.apply(lambda row: row["day"][-5:], axis=1,)
    fdf = fdf.sort_values(by=["day"])
    fig, ax = plt.subplots(figsize=(14, 7))
    sns.barplot(x="day", y="cases", hue="Description", data=fdf, ax=ax).set_title(title)
    fig.savefig("images/{}.png".format(bairro))


save_plot("rio", "2020-03-31", "Rio de Janeiro", 512)
save_plot("copacabana", "2020-03-30", "Copacabana", 512)
save_plot("barra", "2020-03-30", "Barra da Tijuca", 512)
