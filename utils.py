import pickle
from os import makedirs, path


def mkdir(folder):
    if folder[-1] != "/":
        folder = "{}/".format(folder)
    makedirs(folder, exist_ok=True)
    return folder


def valid_tuple_sizes(entry_size, max_value=64):
    valid_values = []
    for tuple_size in range(3, max_value):
        if entry_size % tuple_size == 0:
            valid_values.append(tuple_size)
    return valid_values


def file_exists(filename):
    return path.exists(filename)


def save_data(data, filename):
    with open(filename, "wb") as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load_data(filename):
    with open(filename, "rb") as handle:
        return pickle.load(handle)
