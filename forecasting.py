import pandas as pd
import numpy as np
import wisardpkg as wp
from utils import valid_tuple_sizes
from datamanager import DataManager
from datetime import datetime, timedelta
from sklearn.metrics import mean_absolute_error


def get_rj_data(filename="data/brazil_covid19.csv"):
    df = pd.read_csv(filename)
    rj_df = df[df["state"] == "Rio de Janeiro"].reset_index()
    rj_df.drop(columns=["index", "region", "state"], inplace=True)
    rj_df.set_index("date", inplace=True)
    return rj_df


def calculate_g(cropped_data):
    g = 0.0
    for x_den in cropped_data[:-1]:
        g += 1.0 if x_den == 0 else cropped_data[-1] / x_den
    return g / (len(cropped_data) - 1)


def calculate_x_and_y(data, lag=1):
    x = []
    y = []
    for t in range(lag, len(data) - 1):
        g_x = calculate_g(data[t - lag : t + 1])
        g_y = calculate_g(data[(t - lag) + 1 : t + 2])
        x.append(g_x)
        y.append(g_y)

    t = len(data) - 1
    g_x = calculate_g(data[t - lag : t + 1])
    x.append(g_x)
    return x, y


def predict(data, prefix, therm_size, num_exec, periods, final_date="2020-03-31"):
    # all_tuple_sizes = valid_tuple_sizes(therm_size)
    data["day"] = pd.to_datetime(data["day"], format="%Y-%m-%d")
    mean_funcs = {
        "SimpleMean": wp.SimpleMean(),
        "PowerMean": wp.PowerMean(2),
        # "Median": wp.Median(),
        "HarmonicMean": wp.HarmonicMean(),
        "HarmonicPowerMean": wp.HarmonicPowerMean(2),
        # "GeometricMean": wp.GeometricMean(),
    }
    base_date = datetime.strptime(final_date, "%Y-%m-%d").date()
    cases = data[data["day"] < datetime.strptime(final_date, "%Y-%m-%d")][
        "cases"
    ].values.tolist()
    for mean_name, value in mean_funcs.items():
        manager = DataManager("data", "{}".format(prefix), "th_{}".format(therm_size))
        for lag in [2]:
            x, y = calculate_x_and_y(cases, lag=lag)
            therm = wp.SimpleThermometer(therm_size, 0.1, 2 * np.max(x))
            train_ds = wp.DataSet(
                [therm.transform([x[i]]) for i in range(len(x) - 1)], y
            )
            for tuple_size in [32]:
                for _ in range(int(num_exec / 5)):
                    for steps in [2, 3, 4, 8, 10]:
                        r = wp.RegressionWisard(tuple_size, mean=value, steps=steps)
                        r.train(train_ds)

                        y_ = x[-(lag + 1) :].copy()

                        current_value = cases[-1]
                        for t in range(periods):
                            g_x = calculate_g(y_[len(y_) - (lag + 1) :])
                            g_y_ = r.predict(therm.transform([g_x]))
                            y_.append(g_y_)
                            current_value = current_value * g_y_

                            manager.append(
                                lag,
                                tuple_size,
                                current_value,
                                mean_name,
                                base_date + timedelta(days=t + 1),
                            )

                        manager.save()


def sum_new_cases(filename):
    df = pd.read_csv(filename)
    output = pd.DataFrame(columns=["cases", "day"])
    total_cases = 0
    for _, row in df.iterrows():
        total_cases += row["CONFIRMADOS"]
        output = output.append(
            {"cases": total_cases, "day": row["DT_NOTIFIC"]}, ignore_index=True
        )

    return output


df = pd.read_csv("data/rio_2020-03-31.csv")
predict(
    data=df,
    prefix="cases_rio",
    therm_size=512,
    num_exec=1000,
    periods=18,
    final_date="2020-03-31",
)

df = sum_new_cases("data/COPACABANA_2020-03-30.csv")
df.to_csv("data/copacabana_2020-03-30.csv")
data = df["cases"].values.tolist()
predict(
    data=data,
    prefix="cases_copacabana",
    therm_size=512,
    num_exec=100,
    periods=15,
    final_date="2020-03-30",
)

df = sum_new_cases("data/BARRA_DA_TIJUCA_2020-03-30.csv")
df.to_csv("data/barra_2020-03-30.csv")
data = df["cases"].values.tolist()
predict(
    data=data,
    prefix="cases_barra",
    therm_size=512,
    num_exec=100,
    periods=15,
    final_date="2020-03-30",
)
