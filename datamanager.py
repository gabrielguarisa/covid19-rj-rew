from utils import mkdir, file_exists
import pandas as pd


class DataManager:
    def __init__(self, folder, dataset_name, binarization_name):
        self.folder = mkdir(folder)
        self.dataset_name = dataset_name
        self.binarization_name = binarization_name

        self.load()

    def get_filename(self):
        return "{}/{}_{}.csv".format(
            self.folder, self.dataset_name, self.binarization_name
        )

    def get_data(self):
        return self._data

    def load(self):
        if file_exists(self.get_filename()):
            self._data = pd.read_csv(self.get_filename())
        else:
            self._data = pd.DataFrame(columns=["lag", "n", "y", "mean", "day"])
            self._data = self._data.astype(
                {"lag": int, "n": int, "y": float, "mean": str, "day": int}
            )
        return self

    def save(self):
        self._data.to_csv(self.get_filename(), index=False)
        return self

    def append(self, lag, tuple_size, y, mean_func, day):
        self._data = self._data.append(
            {"lag": lag, "n": tuple_size, "y": y, "mean": mean_func, "day": day},
            ignore_index=True,
        )
        return self

    def get_filtered_data(
        self, lag=None, tuple_size=None, y=None, mean_func=None, day=None
    ):
        fdf = self._data.copy()

        if lag != None:
            fdf = fdf.loc[fdf["lag"] == lag]

        if tuple_size != None:
            fdf = fdf.loc[fdf["n"] == tuple_size]

        if y != None:
            fdf = fdf.loc[fdf["y"] == y]

        if mean_func != None:
            fdf = fdf.loc[fdf["mean"] == mean_func]

        if day != None:
            fdf = fdf.loc[fdf["day"] == day]

        return fdf
